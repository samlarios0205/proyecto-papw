**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---
## Descripción de Primer Avance
Este avance esta constituido por:

1. 6 doumentos .html
2. 1 elemento .css 
3. 1 elemento .js

---
## Páginas HTML
### Página Principal (MultiMart.html)
Antes de describir la página hay que tener en cuenta lo siguiente:
Los Elementos que todas las páginas html tienen en comun son:

1. Un menu de navegación en el top de la página, junto con un textbox de Busqueda y el nombre de la página en posición Fixed para simular un menu pegajoso.
2. Un menu lateral que se activa cuando se da clic al botón "Más" del menu de navegación.
3. Varios elementos recursivos (Que se adaptan al tamaño de la página.

La página principal de la tienda se asemeja a la ventana principal de la página wish.com, donde se muestran los productos en forma de tarjetas, dichas tarjetas tienen:

1. El nombre del producto.
2. Precio y valoración de corazones.
3. Imagen Preview.

La unica tarjeta que funciona es la primera que anuncia la teblata Wacom Intuos Manga, el cual de lleva a otra página (donde se muestran los datos del producto.

#### Ventana Login
La ventana login es una modal que aparece en la misma página principal, esta se puede mostrar al dar clic en la opción "Nombre de Usuario" del menu de navegación.

Esta aparecera en frente, dejando el fondo más opaco y te pedira llenar tus datos para registrate, esta compuesto de:

1. Los textbox correspondientes para el nickname/correo del usuario y su contraseña.
2. Un checkbox con la leyenda "Recuerdame"
3. Dos botones, uno de submit que solo lo hara cuando se llenen los datos correspondientes de forma correcta y otro de registro que te lleva a la página de registro.

---
### Página de Registro (RegistrarUsuario.html)
Esta página es un form completo donde se tienen que llenar los datos para poder registrarse a la tienda con los campos:

1. Nombre y Apellido.
2. Correo Electronico.
3. Telefono y Dirección.
4. Nickname.
5. Contraseña y confirmación de contraseña.
6. Foto de Perfil y Foto de Portada (los cuales no son opcionales y puedes poner incluso cuando ya estas registrado.

Si no se cumplen las validaciones, te mandaran mensajes en donde debes corregir, si si los llenaste bien, le mandara a tu Perfil de Usuario.

---
### Perfil de Usuario (UsuarioPerfil.html)
Esta página esta constituida por:

1. La imagen de Portada.
2. Un menu en el lado izquierdo con la foto de perfil del usuario, este mismo menu te lleva a diferentes tabs y páginas depedniendo de la opción.
3. Los Tabs serían el Carrito, El Historial de Compra y Los borradores, los cuales se pueden ver al dar clic en la opción y en el codigo.
4. Las otras opciones abren páginas distintas: La opción de configuración abre la página de registro PERO para editar información y no para registrar; la opción de Venta que te lleva al formulario de Venta y el de salir de Sesión.

---
### Página de Venta (ProductoVender.html)
Como se habia mencionado en la ventana anterior, esta ventana solo se puede acceder cuando se entra en su perfil (ya que solo los usuarios pueden vender).
Esta ventana es similar a la ventana de registro, ya que este es un Formulario.
Esta compuesta por los campos:

1. Nombre y Descripción del Producto.
2. Precio y Unidades.
3. RadioButtons para indicar si es un producto Nuevo o Usado.
4. Checkboxes para indicar en que cual o cuales categorías pertenece el producto.
5. Inputs de archivos (Uno Multiple para las imagenes y otro individual para el video).

---
### Muestra de Producto (ProductoView.html)
Es una ventana donde se mostrará la información de un producto en especifico, esta ventana aparece cuando das clic a la miniatura de un producto (en este caso, el de la tableta)
El cual contiene:

1. Información del producto.
2. Formulario de Compra y de Agregar a Carrito.
3. Imagen principal, miniatura de imagenes secundarias y un video.
4. Sección de comentarios.

---
### Página de Catalogo/Busqueda/Etc. (Catalogo.html)
Es una página template, esta página se utilizara para cuando se requiera buscar por catalogo o buscador, ya que viendo las páginas de compras, todas tienen el mismo diseño.
Es similar a la página principal, pero sin el banner y con información de Busqueda.

