// Validaciones y Funciones extras--------------------------------------------------------------------

// Validaciones Registro
function validate() {
  console.clear();

  // --Validación de Password--
  var passInput = document.getElementById("userPassword");  
  var confirmInput = document.getElementById("userConfirm");  
  var lowerCase = /[a-z]/g;
  var upperCase = /[A-Z]/g;
  var number = /[0-9]/g;  
  var Valid = false;

  if (passInput.value.length > 8 &&
      passInput.value.match(lowerCase) &&
      passInput.value.match(upperCase) &&
      passInput.value.match(number)) {
    Valid = true;
  }

  if (!Coincide()) {
    alert ('Favor de asegurarse de que las contraseñas coincidan.');
    return false;
    
  }

  if (!Valid) {
    alert ('La contraseña debe de ser de minimo 8 digitos incluyendo una mayuscula, una minuscula y un número.');
    return false;
  } 
}

function Coincide() {
  var passInput = document.getElementById("userPassword").value;  
  var passConfirm = document.getElementById("userConfirm").value;
  
  if (passInput != '' && passInput != passConfirm) {
		return false;
  }
  return true;
}

// Ventana Login


// Menu Responsivo
function responsiveTopMenu() {
    var x = document.getElementById("myMenu");
    if (x.className === "menu") 
    {
        x.className += " responsive";
    } 
    else 
    {
        x.className = "menu";
    }
  }

// Abrir Menu Deslizante
function openSideMenu() {
    document.getElementById("my-SideMenu").style.width = "250px"; 
    document.getElementById("global").style.marginLeft = "250px";
}

function closeSideMenu() {
    document.getElementById("my-SideMenu").style.width = "0";
    document.getElementById("global").style.marginLeft = "0px";
}
  
// "Sticky" Menu
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
      document.getElementById("stick").style.top = "0";
     
    } else {
      document.getElementById("stick").style.top = "-85px";
    }
  prevScrollpos = currentScrollPos;
  } 